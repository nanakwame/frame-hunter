import portraitFrame1 from "./portraitFrames/frame1.png";
import portraitFrame2 from "./portraitFrames/frame2.png";
import portraitFrame3 from "./portraitFrames/frame3.png";
import portraitFrame4 from "./portraitFrames/frame4.png";
import portraitFrame5 from "./portraitFrames/frame5.png";
import portraitFrame6 from "./portraitFrames/frame6.png";
import portraitFrame7 from "./portraitFrames/frame7.png";
import portraitFrame8 from "./portraitFrames/frame8.png";
import portraitFrame9 from "./portraitFrames/frame9.png";
import portraitFrame10 from "./portraitFrames/frame10.png";
import portraitFrame11 from "./portraitFrames/frame11.png";
import portraitFrame12 from "./portraitFrames/frame12.png";
import portraitFrame13 from "./portraitFrames/frame13.png";
import portraitFrame14 from "./portraitFrames/frame14.png";
import portraitFrame15 from "./portraitFrames/frame15.png";
import portraitFrame16 from "./portraitFrames/frame16.png";

import landscapeFrame1 from "./landscapeFrames/landscape1.png";
import landscapeFrame2 from "./landscapeFrames/landscape2.png";
import landscapeFrame3 from "./landscapeFrames/landscape3.png";
import landscapeFrame4 from "./landscapeFrames/landscape4.png";
import landscapeFrame5 from "./landscapeFrames/landscape5.png";
import landscapeFrame6 from "./landscapeFrames/landscape6.png";
import landscapeFrame7 from "./landscapeFrames/landscape7.png";
import landscapeFrame8 from "./landscapeFrames/landscape8.png";
import landscapeFrame9 from "./landscapeFrames/landscape9.png";
import landscapeFrame10 from "./landscapeFrames/landscape10.png";
import landscapeFrame11 from "./landscapeFrames/landscape11.png";
import landscapeFrame12 from "./landscapeFrames/landscape12.png";
import landscapeFrame13 from "./landscapeFrames/landscape13.png";
import landscapeFrame14 from "./landscapeFrames/landscape14.png";
import landscapeFrame15 from "./landscapeFrames/landscape15.png";
import landscapeFrame16 from "./landscapeFrames/landscape16.png";

export const frameProperties = [
  {
    landscapeImagePath: landscapeFrame1,
    portraitImagePath: portraitFrame1,
    frameSizeProperty: [
      {
        size: "SMALL",
        price: "25",
        dimensions: "5 x 7",
      },
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame2,
    portraitImagePath: portraitFrame2,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame3,
    portraitImagePath: portraitFrame3,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame4,
    portraitImagePath: portraitFrame4,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame5,
    portraitImagePath: portraitFrame5,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame6,
    portraitImagePath: portraitFrame6,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame7,
    portraitImagePath: portraitFrame7,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame8,
    portraitImagePath: portraitFrame8,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame9,
    portraitImagePath: portraitFrame9,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame10,
    portraitImagePath: portraitFrame10,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame11,
    portraitImagePath: portraitFrame11,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame12,
    portraitImagePath: portraitFrame12,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame13,
    portraitImagePath: portraitFrame13,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame14,
    portraitImagePath: portraitFrame14,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame15,
    portraitImagePath: portraitFrame15,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
  {
    landscapeImagePath: landscapeFrame16,
    portraitImagePath: portraitFrame16,
    frameSizeProperty: [
      {
        size: "MEDIUM",
        price: "50",
        dimensions: "10 x 12",
      },
      {
        size: "LARGE",
        price: "100",
        dimensions: "12 x 16",
      },
      {
        size: "EXTRA_LARGE",
        price: "150",
        dimensions: "20 x 30",
      },
      {
        size: "EXTRA_EXTRA_LARGE",
        price: "200",
        dimensions: "24 x 36",
      },
      {
        size: "EXTRA_EXTRA_EXTRA_LARGE",
        price: "250",
        dimensions: "24 x 36",
      },
    ],
  },
];
