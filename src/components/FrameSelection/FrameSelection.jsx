import React, { useState } from "react";
import "./FrameSelection.css";
import classNames from "classnames";
import { frameProperties } from "../../frames/frameProperty";
import { startCase, toLower } from "lodash";
import defaultPlaceholder from "../../frames/default-placeholder.png";

export const FrameSelection = () => {
  const [selectedFrame, setSelectedFrame] = useState(0);
  const [selectedOrientation, setSelectedOrientation] = useState("PORTRAIT");
  const [frameSize, setFrameSize] = useState("MEDIUM");
  const [selectedPicture, setSelectedPicture] = useState(undefined);

  const sizeTag = {
    SMALL: {
      tag: "S",
      height: "100px",
      width: "130px",
    },
    MEDIUM: {
      tag: "M",
      height: "220px",
      width: "180px",
    },
    LARGE: {
      tag: "L",
      height: "250px",
      width: "210px",
    },
    EXTRA_LARGE: {
      tag: "XL",
      height: "270px",
      width: "230px",
    },
    EXTRA_EXTRA_LARGE: {
      tag: "XXL",
      height: "290px",
      width: "260px",
    },
    EXTRA_EXTRA_EXTRA_LARGE: {
      tag: "3XL",
      height: "300px",
      width: "280px",
    },
  };

  const renderFrameSelector = () => (
    <div className="framesSelector">
      {frameProperties.map((frame, index) => (
        <img
          onClick={() => setSelectedFrame(index)}
          className={classNames({ activeFrame: selectedFrame === index })}
          src={frame.portraitImagePath}
          width="90px"
          height="90px"
          alt="Test Frame"
        />
      ))}
    </div>
  );

  const imageUploadPreviewSection = () => (
    <div className="imageUploadPreviewSection">
      <div className="selectedFrameContainer">
        {renderFrameSelectionComponent()}
      </div>
      <div>{renderSizeOrientationAndInformationDisplay()}</div>
      <div />
      <div>{renderImageUploaderPreview()}</div>
    </div>
  );

  const renderFrameSelectionComponent = () => (
    <div>
      <img
        className="renderFrameSelectionComponent"
        src={
          selectedOrientation === "PORTRAIT"
            ? frameProperties[selectedFrame].portraitImagePath
            : frameProperties[selectedFrame].landscapeImagePath
        }
        width={
          sizeTag[
            frameProperties[selectedFrame].frameSizeProperty.find(
              (x) => x.size === frameSize
            )?.size
          ]?.width
        }
        height={
          sizeTag[
            frameProperties[selectedFrame].frameSizeProperty.find(
              (x) => x.size === frameSize
            )?.size
          ]?.height
        }
        alt="Selected Frame"
      />
    </div>
  );

  const renderSizeOrientationAndInformationDisplay = () => (
    <div className="renderSizeOrientationAndInformationDisplay">
      <div>{renderOrientationPictureSelection()}</div>
      <div>{renderOrientationSizeSelection()}</div>
      <div>{renderDetailsOfChoiceSelected()}</div>
      <div>{renderPriceOfSelectedFrame()}</div>
    </div>
  );

  const renderOrientationPictureSelection = () => (
    <div className="summarySection">
      <div className={classNames("frameSelectionSummaryTitle", "font-sans")}>
        What's your picture orientation ?
      </div>

      <div className="orientationPictureSelection">
        <div
          className={classNames("portraitDisplay", {
            activeFrame: selectedOrientation === "PORTRAIT",
          })}
          onClick={() => {
            setSelectedOrientation("PORTRAIT");
          }}
        >
          <div>Portrait</div>
        </div>

        <div
          className={classNames("landscapeDisplay", {
            activeFrame: selectedOrientation === "LANDSCAPE",
          })}
          onClick={() => {
            setSelectedOrientation("LANDSCAPE");
          }}
        >
          Landscape
        </div>
      </div>
    </div>
  );

  const renderOrientationSizeSelection = () => (
    <div>
      <div className={classNames("frameSelectionSummaryTitle", "font-sans")}>
        How Big Do You Want It?
      </div>

      <div className="frameSizeSelection">
        {frameProperties[selectedFrame].frameSizeProperty.map((frame) => (
          <div>
            <div
              className={classNames("frameSizeSelectionCta", {
                activeFrame: frameSize === frame.size,
              })}
              onClick={() => setFrameSize(frame.size)}
            >
              <span className={"frameSizeText"}>{sizeTag[frame.size].tag}</span>
            </div>
            <div className="frameSizeDescription">{frame.dimensions}</div>
          </div>
        ))}
      </div>
    </div>
  );

  const renderDetailsOfChoiceSelected = () => (
    <div>
      <div className={classNames("frameSelectionSummaryTitle", "font-sans")}>
        Details
      </div>

      <div className={classNames("detailsDescription", "font-sans")}>
        {`Orientation : ${startCase(toLower(selectedOrientation))} ${
          sizeTag[
            frameProperties[selectedFrame].frameSizeProperty.find(
              (x) => x.size === frameSize
            )?.size
          ]?.tag
        }`}
      </div>

      <div className={classNames("detailsDescription", "font-sans")}>
        {`Dimension : ${
          frameProperties[selectedFrame].frameSizeProperty.find(
            (x) => x.size === frameSize
          )?.dimensions
        }`}
      </div>
    </div>
  );

  const renderPriceOfSelectedFrame = () => (
    <div>
      <div className={classNames("frameSelectionSummaryTitle", "font-sans")}>
        Price
      </div>

      <div className={classNames("priceDescription", "font-sans")}>
        {`GHS ${
          frameProperties[selectedFrame].frameSizeProperty.find(
            (x) => x.size === frameSize
          )?.price
        }`}
      </div>
    </div>
  );

  const renderImageUploaderPreview = () => (
    <div className="imageUploaderPreviewSection">
      <div>
        <input
          type="file"
          id="pictureUpload"
          onChange={(e) => setSelectedPicture(e.target.files[0])}
        />
        <div className="pictureUploader">
          <label htmlFor="pictureUpload">Upload Your Picture</label>
        </div>
      </div>
      <div>
        {
          <img
            src={
              selectedPicture
                ? URL.createObjectURL(selectedPicture)
                : defaultPlaceholder
            }
            alt={"Preview"}
            className="imagePreviewer"
          />
        }
      </div>
    </div>
  );

  return (
    <div className="main">
      <div className={classNames("frameSelectorTitle", "font-sans")}>
        Select Frame
      </div>

      <div>{renderFrameSelector()}</div>
      <div>{imageUploadPreviewSection()}</div>
    </div>
  );
};
