import "./App.css";
import { FrameSelection } from "./components/FrameSelection/FrameSelection";
import { Header } from "./components/Navigation/Header";

function App() {
  return (
    <div className="App h-screen flex flex-col">
      <Header />
      <div className="flex-1 overflow-y-auto">
        <FrameSelection />
      </div>
    </div>
  );
}

export default App;
